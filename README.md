# powerdevil-light - plasma beta 5.20.90

PKGBUILD and Jonathan Riddel's gpg file to build powerdevil without network manager dependancy

to build first import the provided gpg key
```
gpg --import jriddell.gpg
```
then proceed to build the package with makepkg or some aur helper (I recommend aur utils)
